#!/usr/bin/env bash
random_email="$(openssl rand -hex 12)"@example.com
curl https://get.acme.sh | sh -s email=${random_email}
